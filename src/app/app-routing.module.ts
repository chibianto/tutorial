import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'esempio',
    loadChildren: () => import('./esempio/esempio.module').then( m => m.EsempioPageModule)
  },
  {
    path: 'ciao',
    loadChildren: () => import('./ciao/ciao.module').then( m => m.CiaoPageModule)
  },
  {
    path: 'md-menu',
    loadChildren: () => import('./md-menu/md-menu.module').then( m => m.MdMenuPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
