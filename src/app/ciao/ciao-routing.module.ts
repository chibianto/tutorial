import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CiaoPage } from './ciao.page';

const routes: Routes = [
  {
    path: '',
    component: CiaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CiaoPageRoutingModule {}
