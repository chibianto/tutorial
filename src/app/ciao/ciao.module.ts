import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CiaoPageRoutingModule } from './ciao-routing.module';

import { CiaoPage } from './ciao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CiaoPageRoutingModule
  ],
  declarations: [CiaoPage]
})
export class CiaoPageModule {}
