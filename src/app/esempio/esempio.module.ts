import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EsempioPageRoutingModule } from './esempio-routing.module';

import { EsempioPage } from './esempio.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EsempioPageRoutingModule
  ],
  declarations: [EsempioPage]
})
export class EsempioPageModule {}
